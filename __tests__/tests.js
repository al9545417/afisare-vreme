const request = require("supertest");
const app = require("../app");

describe("Test the root path", () => {
    test("It should response the GET method", async () => {
        const response = await request(app).get("/");
        expect(response.statusCode).toBe(200);
    });
});

describe("Test the cities json object", () => {
    test("It should response with a city object", async () => {
        const response = await request(app).get(`/check-city/bucharest`);
        expect(response.statusCode).toBe(200);
    });

    test("It should response with a 404 status code if city doesn't exists", async () => {
        const response = await request(app).get(`/check-city/berlin`);
        expect(response.statusCode).toBe(404);
    });
});

describe("Test city weather data", () => {
    test("It should response with a weather object", async () => {
        const response = await request(app).get(`/v1/weather/bucharest`);
        expect(response.statusCode).toBe(200);
        expect(response.body.name).toBe("BUCHAREST")
    });

    test("It should response with html page with weather data", async () => {
        const response = await request(app).get(`/weather/bucharest`);

        expect(response.type).toBe("text/html");
    });

    // test("It should fail", async () => {
    //     expect(0).ToBe(1)
    // })
});

