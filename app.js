const express = require('express');
const app = express();
const port = 3000;
//api-key: 29f0876752ac53a41304d3fcbb1b6e7c
const { getCityByName } = require("./utils");
const { default: axios } = require('axios');

const apiKey = process.env.API_KEY ?? "29f0876752ac53a41304d3fcbb1b6e7c"

console.log(process.env.API_KEY)

const cities = {
    "bucharest": {
        name: ["bucharest", "bucurest"],
        lat: 44.439663,
        long: 26.096306
    },
    "constanta": {
        name: ["constanta"],
        lat: 44.179249,
        long: 28.649940
    }
}

app.get('/', (req, res) => {
    res.send('Afisare Vreme');
});


app.get('/v1/weather/:name', async (req, res) => {
    const cityName = req.params.name

    if (!cityName) {
        return res.status(404).send({
            message: "City invalid!"
        })
    }

    const cityData = getCityByName(cityName, cities)

    if (!cityData) {
        return res.status(404).send({
            message: "City not found!"
        })
    }

    const { lat, long } = cityData

    const url = `https://api.openweathermap.org/data/2.5/weather?units=metric&lat=${lat}&lon=${long}&appid=${apiKey}`
    try {
        const response = await axios.get(url)
        const weatherData = response.data;

        console.log(`Weather in ${cityData.name[0]}:`);
        console.log(`- Description: ${weatherData.weather[0].description}`);
        console.log(`- Temperature: ${weatherData.main.temp}°C`);
        console.log(`- Humidity: ${weatherData.main.humidity}%`);
        console.log(`- Wind Speed: ${weatherData.wind.speed} m/s`);

        return res.status(200).send({
            name: cityData.name[0].toUpperCase(),
            lat,
            long,
            description: weatherData.weather[0].description,
            temperature: `${weatherData.main.temp}°C`,
            humidity: `${weatherData.main.humidity}%`,
            windSpeed: `${weatherData.wind.speed} m/s`
        })
    } catch (err) {
        return res.status(500).send({
            message: "Unexpected error"
        })
    }
})

app.get('/weather/:name', async (req, res) => {
    const cityName = req.params.name

    if (!cityName) {
        return res.status(404).send("City invalid.")
    }

    const cityData = getCityByName(cityName, cities)

    if (!cityData) {
        return res.status(404).send("City not found.")
    }

    const { lat, long } = cityData

    const url = `https://api.openweathermap.org/data/2.5/weather?units=metric&lat=${lat}&lon=${long}&appid=${apiKey}`
    try {
        const response = await axios.get(url)
        const weatherData = response.data;

        console.log(`Weather in ${cityData.name[0]}:`);
        console.log(`- Description: ${weatherData.weather[0].description}`);
        console.log(`- Temperature: ${weatherData.main.temp}°C`);
        console.log(`- Humidity: ${weatherData.main.humidity}%`);
        console.log(`- Wind Speed: ${weatherData.wind.speed} m/s`);

        return res.status(200).send(`
            <h1> Weather in <span style="text-transform:uppercase">${cityData.name[0]}</span>: </h1>
            <p> - Description: ${weatherData.weather[0].description} </p>
            <p> - Temperature: ${weatherData.main.temp}°C </p>
            <p> - Humidity: ${weatherData.main.humidity}% </p>
            <p> - Wind Speed: ${weatherData.wind.speed} m/s </p>
        `)

    } catch (err) {
        return res.status(500).send("Unexpected error")
    }
})

app.get('/check-city/:name', (req, res) => {
    const cityName = req.params.name

    if (!cityName) {
        return res.status(404).send({
            message: "City invalid."
        })
    }

    const cityData = getCityByName(cityName, cities)

    if (!cityData) {
        return res.status(404).send({
            message: "City not found!"
        })
    }

    return res.status(200).send(cityData)
})


module.exports = app