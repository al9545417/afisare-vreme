
/**
 * 
 * @param {string} cityName 
 * @param {Object} cities 
 * @returns {Object}
 */
function getCityByName(cityName, cities) {
    cityName = cityName.toLowerCase();
    for (const cityKey in cities) {
        if (cities.hasOwnProperty(cityKey)) {
            const city = cities[cityKey];
            if (city.name.includes(cityName)) {
                return { name: cityKey, ...city };
            }
        }
    }
    return undefined;
}


module.exports = {
    getCityByName
}